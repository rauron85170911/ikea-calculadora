import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BackendService } from '../backend.service';
import { UtilsService } from '../mycommon/utils.service';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss']
})
export class UploadFileComponent implements OnInit {

  @Output() facturaUploaded: EventEmitter<any> = new EventEmitter<any>();
  constructor(private servicio: BackendService, private utils: UtilsService) { }

  mostrandoInstrucciones = false;
  mensajeInformacion = 'Información sobre el montaje del producto';

  ngOnInit() {
  }
  public handleFileInput($event) {
    // this.utils.StartLoading();

    this.servicio.UploadPdfFactura($event[0]).subscribe(data => {
      // this.utils.StopLoading();
      this.facturaUploaded.emit(data);

    }, error => {
      // this.utils.StopLoading();
      this.utils.Error(error);
    });




  }
}
