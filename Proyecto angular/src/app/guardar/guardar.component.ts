import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { BackendService } from '../backend.service';
import { ModalService } from '../mycommon/modal.service';

@Component({
  selector: 'app-guardar',
  templateUrl: './guardar.component.html',
  styleUrls: ['./guardar.component.css']
})
export class GuardarComponent implements OnInit {

  error = null;
  get f() { return this.formulario.controls; }
  formulario: FormGroup;
  submitted = false;
  @Input() factura: any = {};
  // @Output()contratacionrealizada: EventEmitter<any> = new EventEmitter<any>();
  constructor(private formBuilder: FormBuilder, private servicio: BackendService, private modalservice: ModalService) { }


  ngOnInit() {
    this.formulario = this.formBuilder.group({
      nombre: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  public cancelar() {
    this.modalservice.close('guardado', true, null);
  }
  public onGuardar() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.formulario.invalid) {
        return;
    }

    // si el formulario es correcto

    this.servicio.Guardar(this.factura,
                            this.f.nombre.value,
                            this.f.email.value).subscribe(
    response => {
        // si la respuesta es correcta, emitimos el evento de contratacion realizada
        this.modalservice.close('guardado', false, response);
        // this.contratacionrealizada.emit();
    },
    error => {
      this.error = error;
    });

  }

}
