import { Component, OnInit } from '@angular/core';
import { BackendService } from './backend.service';
import { UtilsService } from './mycommon/utils.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  mensaje = 'inicial';
  title = 'calculadora';
  step = 1;
  factura: any = {};
  contratacionrealizada = 'false';
  constructor(private servicio: BackendService, private utils: UtilsService) { }

  ngOnInit(): void {
    // this.step = 2;
    // this.servicio.GetFacturaMock(null).subscribe(data => {
    //   this.factura = data;
    // });
  }

  gotoPaso2($event) {
    this.factura = $event;
    this.step = 2;
  }
  gotoPaso1() {
    this.step = 1;
    this.factura = {};
  }


  gotoPaso3($event) {

    this.contratacionrealizada = $event.contratacion;
    this.step = 3;
  }

}
