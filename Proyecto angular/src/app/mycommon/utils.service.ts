import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { EventEmitter } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(private spinner: NgxSpinnerService) {
    this.Errors  = new EventEmitter<any>();
    this.Messages = new EventEmitter<any>();
  }

  public Errors: EventEmitter<any>;
  public Messages: EventEmitter<any>;

  public Error(mensaje: string) {
    console.error(`Se ha producido un error:${mensaje}`);
    this.Errors.emit(mensaje);
  }

  public ShowMessage(titulo: string, descripcion: string) {
    this.Messages.emit({titulo: titulo, descripcion: descripcion});
  }

  public StartLoading() {
    console.log('Se muestra el spinner de loading');
    this.spinner.show();

    // setTimeout(() => {
    //     /** spinner ends after 5 seconds */

    // }, 5000);
  }
  public StopLoading() {
    this.spinner.hide();
    console.log('Se oculta el spinner de loading');
  }

  public newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        // tslint:disable-next-line:no-bitwise
        const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
}
