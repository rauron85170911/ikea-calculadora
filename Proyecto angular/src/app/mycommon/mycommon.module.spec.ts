import { MyCommonModule } from './mycommon.module';

describe('MyCommonModule', () => {
  let commonModule: MyCommonModule;

  beforeEach(() => {
    commonModule = new MyCommonModule();
  });

  it('should create an instance', () => {
    expect(commonModule).toBeTruthy();
  });
});
