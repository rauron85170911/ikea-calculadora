import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MensajePopupComponent } from './mensaje-popup/mensaje-popup.component';
import { ModalPopupComponent } from './modal-popup/modal-popup.component';
import { SoloMarcaPipe } from './solo-marca.pipe';
import { SoloHtmlPipe } from './solo-HTML.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
    declarations: [MensajePopupComponent, ModalPopupComponent, SoloMarcaPipe, SoloHtmlPipe],
    exports: [MensajePopupComponent, ModalPopupComponent, SoloMarcaPipe, SoloHtmlPipe]
})
export class MyCommonModule { }
