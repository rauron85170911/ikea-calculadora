import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajePopupComponent } from './mensaje-popup.component';

describe('MensajePopupComponent', () => {
  let component: MensajePopupComponent;
  let fixture: ComponentFixture<MensajePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensajePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
