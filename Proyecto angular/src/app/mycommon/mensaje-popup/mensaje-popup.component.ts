import { UtilsService } from './../utils.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mensaje-popup',
  templateUrl: './mensaje-popup.component.html',
  styleUrls: ['./mensaje-popup.component.css']
})
export class MensajePopupComponent implements OnInit {

  showing = false;
  titulo = 'Oooops! :(';
  mensaje = 'Mensaje inicial';
  constructor(private utils: UtilsService) {

  }

  ngOnInit() {

    this.utils.Errors.subscribe(mensaje => {
      this.showing = true;
      this.mensaje = mensaje;
    });
  }

}
