import { Injectable, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ModalService {

    private modals: any[] = [];

    private emisor: Subject<any>  = new Subject<any>();

    public add(modal: any) {
        // add modal to array of active modals
        this.modals.push(modal);
    }

    public remove(id: string) {
        // remove modal from array of active modals
        this.modals = this.modals.filter(x => x.id !== id);
    }

    public open(id: string): Subject<any> {
        // open modal specified by id

        const modal: any = this.modals.filter(x => x.id === id)[0];
        modal.open();
        return this.emisor;
    }

    public close(id: string, cancel: boolean, output: any) {
        // close modal specified by id
        const modal: any = this.modals.filter(x => x.id === id)[0];
        modal.close();
        this.emisor.next(
            {
            id: id,
            cancel: cancel,
            output: output
        });


    }
}
