import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'soloMarca'
})
export class SoloMarcaPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const r = /([^a-z]*)/;
    const m = r.exec(value);
    return m[1];
  }

}
