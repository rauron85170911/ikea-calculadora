import { MyCommonModule } from './../mycommon/mycommon.module';
import { Component, OnInit, Input, Output, EventEmitter, TemplateRef, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { BackendService } from '../backend.service';
import { UtilsService } from '../mycommon/utils.service';
import { ModalService } from '../mycommon/modal.service';
import { Observable, Subscription } from 'rxjs';




@Component({
  selector: 'app-fill-data',
  templateUrl: './fill-data.component.html',
  styleUrls: ['./fill-data.component.scss']
})
export class FillDataComponent implements OnInit {
  @Output() atras: EventEmitter<any> = new EventEmitter<any>();
  @Output() procesofinalizado: EventEmitter<any> = new EventEmitter<any>();
  @Input() factura: any;

  @ViewChild('main') main: ElementRef;
  @ViewChild('sidebar') sidebar: ElementRef;
  @ViewChild('faqs') faqs: ElementRef;
  @ViewChild('calculadora') calculadora: ElementRef;

  contratando = false;
  mensajeInformacion = 'Informacion sobre la contratación del producto';


  todoSeleccionado = true;
  constructor(private renderer: Renderer2, private service: BackendService,
       private utils: UtilsService, private modalservice: ModalService) { }

  ngOnInit() {
    this.recalcularAlturaMinima();
  }

  public seleccionarTodo($event) {
    this.factura.Lista.forEach(element => {

        element.UnidadesMontaje = $event.target.checked ? element.Cantidad : 0;

    });
    this.recalcular();
  }
  public recalcular() {
    console.log('Se ha cambiado algún dato, llamamos al backend con los datos');
    // antes de llamar a servidor, miro el estado en que debe quedar el check de seleccionar todo
    this.todoSeleccionado = this.factura.Lista.every(item => {
      // console.log(`el valor de todo seleccionado es:'${this.todoSeleccionado}`);
      return item.Cantidad === item.UnidadesMontaje;
    });

   this.factura.TotalArticulos = 0;
   this.factura.TotalArticulos = this.factura.Lista.filter(item => item.Montaje).map(function (producto) {
     return producto.UnidadesMontaje; }).reduce((sum, elem) => sum + parseInt(elem));

    this.utils.StartLoading();
    this.service.CalcularPrecioMontaje(this.factura).subscribe(
      (data) => {
        this.utils.StopLoading();
        // pasamos el dato a la factura
          this.factura.TotalMontajeFactura = data;
      },
      (error) => {
        this.utils.StopLoading();
        this.utils.Error(error);
      }
    );
  }

  public CambiarFactura() {
    this.atras.emit(null);
  }


  public abrirOpcionGuardar() {
    const s: Subscription = this.modalservice.open('guardado').subscribe(response => {
      s.unsubscribe();
      if (response.cancel) {
        console.log('Se ha cerrado el guardado con cancel');
      } else {
        // aquí lanzamos un evento para que el appcomponent sepa que queremos ir al paso final
        console.log('Se ha guardado el presupuesto correctamente');
        this.procesofinalizado.emit({
          contratacion: false
        }
        );

      }

    });
  }
  public abrirOpcionContratar() {
    const s: Subscription = this.modalservice.open('contratacion').subscribe(response => {
      s.unsubscribe();
      if (response.cancel) {
        console.log('Se ha cerrado la contratación con cancel');
      } else {
        // aquí lanzamos un evento para que el appcomponent sepa que queremos ir al paso final
        console.log('Se ha contratado correctamente');
        this.procesofinalizado.emit({
          contratacion: true
        }
        );
      }
    });
  }

  public recalcularAlturaMinima() {
    const anchoVentana = window.innerWidth;
    const altoVentana = window.innerHeight;
    // console.log(`El ancho de la ventana del dispositivo es${anchoVentana}`);
    /* miramos el alto del sidebar */
    if (anchoVentana > 992) {
      const altocalculadora = this.calculadora.nativeElement.clientHeight;
      // console.log(`Alto calculadora:${altocalculadora}`);
      const altofaqs = this.faqs.nativeElement.clientHeight;
      // console.log(`Alto faqs:${altofaqs}`);
      const total = altocalculadora + altofaqs + 175;
      const calculoaltofaqs = altoVentana - altocalculadora - 95 - 50 - 60; /* footer :95 cabecera :50 paddings : 60*/
      // console.log(`Alto total:${total}`);
      this.renderer.setStyle(this.main.nativeElement, 'min-height', `${total}px`);
      /*this.renderer.setStyle(this.main.nativeElement, 'height', `${total}px`);*/
      // console.log(`alto de ventana total:${window.innerHeight}`);
      /*if (altoVentana >= 1200) {
        // console.log('mayor o igual a 1200');
        // console.log(`calculo contenedor faqs: ${calculoaltofaqs}`);
        this.renderer.setStyle(this.faqs.nativeElement, 'height', `${calculoaltofaqs}px`);
      } else if (altoVentana >= 1000) {
        // console.log('mayor o igual a 1000');
        // console.log(`calculo contenedor faqs: ${calculoaltofaqs}`);
        this.renderer.setStyle(this.faqs.nativeElement, 'height', `${calculoaltofaqs}px`);
      } else if (altoVentana >= 900) {
        // console.log('mayor o igual a 900');
        // console.log(`calculo contenedor faqs: ${calculoaltofaqs}`);
        this.renderer.setStyle(this.faqs.nativeElement, 'height', `${calculoaltofaqs}px`);
      } else if (altoVentana >= 800) {
        // console.log('mayor o igual a 800');
        // console.log(`calculo contenedor faqs: ${calculoaltofaqs}`);
        this.renderer.setStyle(this.faqs.nativeElement, 'height', `${calculoaltofaqs}px`);
      } else if (altoVentana >= 700) {
        // console.log('mayor o igual a 700');
        // console.log(`calculo contenedor faqs: ${calculoaltofaqs}`);
        this.renderer.setStyle(this.faqs.nativeElement, 'height', `${calculoaltofaqs}px`);
      } else {
        // otras
      }*/
    }
    // miramos el alto del
  }
}
