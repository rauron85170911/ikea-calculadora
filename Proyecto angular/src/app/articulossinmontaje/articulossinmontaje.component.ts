import { Component, OnInit, Input, ViewChild, ElementRef, Renderer2, AfterViewInit, AfterViewChecked } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
@Component({
  selector: 'app-articulossinmontaje',
  templateUrl: './articulossinmontaje.component.html',
  styleUrls: ['./articulossinmontaje.component.css']
})
export class ArticulossinmontajeComponent implements OnInit, AfterViewInit, AfterViewChecked {


  @Input() data: any;
  /*@ViewChild('lnktoggle') lnkToggle: ElementRef;*/
  show = false;
  /* textoCambio = false;*/
  textoArticulos = 'Ver articulos';

  constructor(private renderer: Renderer2) { }

  ngOnInit() {

  }

  ngAfterViewInit(): void {

  }
  ngAfterViewChecked(): void {
      // this.renderer.listen(this.lnkToggle.nativeElement, 'click', (event) => {
      //   this.show = !this.show;
      //   event.stopPropagation();
      // });
  }

  /*toggle() {
    this.show = !this.show;
  }*/
  cambiarTexto(event) {
    this.show = !this.show;
    if (this.show) {
      this.textoArticulos = 'Ocultar Articulos';
      return true;
    } else {
      this.textoArticulos = 'Ver articulos';
      return true;
    }
  }
}
