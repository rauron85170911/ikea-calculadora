import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticulossinmontajeComponent } from './articulossinmontaje.component';

describe('ArticulossinmontajeComponent', () => {
  let component: ArticulossinmontajeComponent;
  let fixture: ComponentFixture<ArticulossinmontajeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticulossinmontajeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticulossinmontajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
