import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { BackendService } from '../backend.service';
import { ModalService } from '../mycommon/modal.service';

@Component({
  selector: 'app-contratar',
  templateUrl: './contratar.component.html',
  styleUrls: ['./contratar.component.css']
})
export class ContratarComponent implements OnInit {


  error = null;
  get f() { return this.formulario.controls; }
  formulario: FormGroup;
  submitted = false;
  @Input() factura: any = {};
  @Output() contratacionrealizada: EventEmitter<any> = new EventEmitter<any>();
  constructor(private formBuilder: FormBuilder, private servicio: BackendService, private modalservice: ModalService) { }



  ngOnInit() {

    this.formulario = this.formBuilder.group({
      nombre: ['', Validators.required],
      telefono: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });

    if (this.factura) {
      this.f.nombre.setValue(this.factura.NombreCliente);
      this.f.email.setValue(this.factura.correoCliente);
    }
  }


  public cancelar() {
    this.modalservice.close('contratacion', true, null);
  }
  public onContratar() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.formulario.invalid) {
      return;
    }

    // si el formulario es correcto

    this.servicio.Contratar(this.factura,
      this.f.nombre.value,
      this.f.telefono.value,
      this.f.email.value).subscribe(
        response => {
          // si la respuesta es correcta, emitimos el evento de contratacion realizada
          this.modalservice.close('guardado', false, response);
          this.contratacionrealizada.emit();
        },
        error => {
          this.error = error;
        });

  }

}
