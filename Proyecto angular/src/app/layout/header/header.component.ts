import { Component, OnInit, HostListener, ViewChild, ElementRef, Renderer2 } from '@angular/core';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private renderer: Renderer2) { }

  @ViewChild('cabecera') cabecera: ElementRef;
  @ViewChild('logo') logo: ElementRef;
  isCollapsed = true;
  usuario: any = null;
  ngOnInit() {

    
  }

  @HostListener('window:scroll', ['$event'])
  doSomethingOnWindowsScroll($event: Event) {

    const scrollOffset = $event.srcElement.children[0].scrollTop;
    const sticky = this.cabecera.nativeElement.offsetHeight;
    if (scrollOffset > sticky ) {
      this.renderer.addClass(this.cabecera.nativeElement, 'sticky');
      this.logo.nativeElement.style.width = 100 + 'px';
    } else {
      this.renderer.removeClass(this.cabecera.nativeElement, 'sticky');
      this.logo.nativeElement.style.width = 120 + 'px';
    }
    //

  }
}
