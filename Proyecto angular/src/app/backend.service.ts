import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment} from '../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class BackendService {

  rootendpoint = environment.apiUrlBase; // 'http://localhost:85';
  constructor(private http: HttpClient) { }


  public UploadPdfFactura(fileToUpload: File): Observable<any> {

    const endpoint = `${this.rootendpoint}/api/facturas/upload`;
    const formData: FormData = new FormData();
    formData.append('fileKey', fileToUpload, fileToUpload.name);
    return this.http
      .post(endpoint, formData).pipe(
        map(this.MapearEntidadServidor),
      catchError(this.handleError));

  }

  public CalcularPrecioMontaje(factura: any): Observable<any> {
    const endpoint = `${this.rootendpoint}/api/facturas/calcular`;
    return this.http.post(endpoint, factura).pipe(
      map(response => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  public Contratar(factura: any, nombre: string, telefono: string, email: string){
    const endpoint = `${this.rootendpoint}/api/facturas/contratar`;
    const data = {
      Factura: factura,
      Contacto: {
        nombre: nombre,
        telefono: telefono,
        email: email
      }
    };
    factura.SinMontaje = null;
    return this.http.post(endpoint, data).pipe(
      map(response => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  public Guardar(factura: any, nombre: string, email: string) {
    const endpoint = `${this.rootendpoint}/api/facturas/guardar`;
    const data = {
      Factura: factura,
      Contacto: {
        nombre: nombre,
        telefono: '',
        email: email
      }
    };
    factura.SinMontaje = null;
    return this.http.post(endpoint, data).pipe(
      map(response => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  public GetFaqs(): Observable<any> {
    return this.http.get('assets/data/faqs.json').pipe(
      map( response => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  handleError(error: HttpErrorResponse) {
    
    let errorMessage = '';

    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      if (error.status === 0) {
        errorMessage = `${error.message}`;
      } else if (error.status === 500 ) {
        errorMessage = `${error.error.ExceptionMessage}`;
      } else {
        errorMessage = `${error.error.Message}`;
      }

    }
    // window.alert(errorMessage);
    return throwError(errorMessage);
  }


  public GetFacturaMock(data: any) {
    const fichero: string = '/assets/data/mock2.json';
    return this.http.get(fichero).pipe(
      map(this.MapearEntidadServidor),
      catchError(this.handleError)
    );
  }


  private MapearEntidadServidor(response): any {
    
    return {
        NumeroFactura: response.NumeroFactura,
        NumeroPedido: response.NumeroPedido,
        Vendedor: response.Vendedor,
        Lista: response.Lista.filter(item => item.Montaje || item.Retirada),
        SinMontaje: response.Lista.filter(item => item.Montaje == false && item.Retirada==false),
      correoCliente: response.correoCliente,
      NombreCliente: response.NombreCliente,
      TotalMontajeFactura: response.TotalMontajeFactura,
        TotalArticulos: (response.Lista.filter(item => item.Montaje).length>0 ?response.Lista.filter(item => item.Montaje).map(function (producto) { return producto.UnidadesMontaje; }).reduce((sum, elem) => sum + parseInt(elem)) :0)
    };
  }


}
