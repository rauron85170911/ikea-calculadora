import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UtilsService } from '../mycommon/utils.service';

@Component({
  selector: 'app-referencia',
  templateUrl: './referencia.component.html',
  styleUrls: ['./referencia.component.css']
})
export class ReferenciaComponent implements OnInit {


  // @Input()montajeseleccionado: boolean;
  // @Output() montajeseleccionadoChange: EventEmitter<any>;
  @Input() data: any;
  @Output() dataChange = new EventEmitter<any>();

  mensajeInformacion = 'Información sobre la retirada colchón / electrodoméstico';
  showsubproductos = false; // para controlar el estado de si estamos mostrando los subproductos
  idcheck = '';
  constructor(private utils: UtilsService) { }

  ngOnInit() {
    this.idcheck = 'checkbox-' + this.utils.newGuid();
  }



  addUnidadMontaje(cantidad) {
    if (cantidad < 0  && this.data.UnidadesMontaje > 0 || cantidad > 0 && this.data.UnidadesMontaje < this.data.Cantidad) {
      this.data.UnidadesMontaje += cantidad;
      this.dataChange.emit();
    }

  }
  cambioSeleccionMontaje($event) {
    this.data.UnidadesMontaje = $event.target.checked ? this.data.Cantidad : 0;
    // this.data.contratarmontaje = $event.target.checked;
    this.dataChange.emit();
  }
  cambioSeleccionRetirada($event) {
        this.data.UsuarioRetirada = $event.target.checked;
    }
}
