import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from './layout/layout.module';
import { UploadFileComponent } from './upload-file/upload-file.component';
import { FillDataComponent } from './fill-data/fill-data.component';
import { SummaryComponent } from './summary/summary.component';
import { ReferenciaComponent } from './referencia/referencia.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FaqsComponent } from './faqs/faqs.component';
import { ArticulossinmontajeComponent } from './articulossinmontaje/articulossinmontaje.component';
import { MyCommonModule} from './mycommon/mycommon.module';
import { LoadingInterceptor } from './interceptors/loading.interceptor';
import { ContratarComponent } from './contratar/contratar.component';
import { GuardarComponent } from './guardar/guardar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    UploadFileComponent,
    FillDataComponent,
    SummaryComponent,
    ReferenciaComponent,
    FaqsComponent,
    ArticulossinmontajeComponent,
    ContratarComponent,
    GuardarComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    MyCommonModule,
    NgxSpinnerModule,
    NgbModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
       useClass: LoadingInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
