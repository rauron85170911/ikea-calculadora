import { Component, OnInit,Input, Output, EventEmitter} from '@angular/core';
import { BackendService } from '../backend.service';
import { ModalService } from '../mycommon/modal.service';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.css']
})
export class FaqsComponent implements OnInit {

  @Output() redim: EventEmitter<any> = new EventEmitter<any>();
  /*@Output() seleccionaTipoFaqs: EventEmitter<any> = new EventEmitter<any>();*/
  @Input() public tipoFaqs: any;
  faqs: any[] = [];
  constructor(private servicio: BackendService,
              private modalservice: ModalService) {}

  ngOnInit() {
    this.servicio.GetFaqs().subscribe(response => {
        this.faqs = response;
        this.faqs.forEach( f => {
          f.collapse = false;
        });
    });
    /*setTimeout(() => {
      this.seleccionaTipoFaqs.emit();
    });*/
  }

  toggle(id: string) {
    this.faqs.forEach( f => {
      if (f.id === id) {
         f.collapse = !f.collapse;
      } else {
        f.collapse = true;
      }
    });
    setTimeout(() => {
      this.redim.emit();
    }, 10);

  }

  toggleIkea(id: string) {
    this.faqs.forEach(f => {
      if (f.id === id) {
        f.collapse = !f.collapse;
     } else {
      /*f.collapse = true;*/
     }
     setTimeout(() => {
      this.redim.emit();
    }, 10);
    });
  }

  public showModalFaqs(id: string) {
    // console.log('pulsaste sobre un enlace a faqs');
    this.modalservice.open(id);
  }
  public closeModalFaqs(id: string) {
    // console.log('cerraste modal de Faqs');
    this.modalservice.close(id, true, null);
  }
}
