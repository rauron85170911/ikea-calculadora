$(document).ready(function() {
  /* Funcionalidad para el menu pegajoso (recogida de la pagina de ikea-contacto)*/
  window.onscroll = function() {myFunction()};
  var header = document.getElementById('headerIKEA');
  var sticky = header.offsetTop;
  function myFunction() {
      if (window.pageYOffset > sticky) {
          //console.log('añade sticky');
          //console.log(window.pageYOffset);
          //console.log(sticky);
          header.classList.add('sticky');
          document.getElementById('imgIKEALogoHeader').style.width = '100px';
      } else {
          //console.log('quita sticky');
          //console.log(window.pageYOffset);
          //console.log(sticky);
          header.classList.remove('sticky');
          document.getElementById('imgIKEALogoHeader').style.width = '120px';
      }
  }

  /* Funcionalidad para comprobar dinamicamente la altura del sidebar y calcular dinamicamente la del listado de productos */
  var $productos = $('#main');
  var $productosAltura = $productos.height();
  var $calculadoraMontaje = $('.contenedor-calculadora-montaje')
  var $preguntasFrecuentes = $('.contenedor-preguntas-frecuentes');
  var $calculadoraAltura = $calculadoraMontaje.height();
  var $preguntasAltura = $preguntasFrecuentes.height();
  var $totalDerecha = $calculadoraAltura + $preguntasAltura + 30;
  var $anchoWindow = $(window).width();
  //console.log('altura contenedor listado :' + $productosAltura);
  //console.log('altura contenedor calculadora :' + $calculadoraAltura);
  //console.log('altura contenedor preguntas f. :' + $preguntasAltura);
  //console.log('ancho ventana :' + $anchoWindow);

  if($anchoWindow >= 992) {
    //console.log('es mayor de 992px');
    //console.log($totalDerecha);
    if($productosAltura < $totalDerecha) {
      $productos.css('min-height', $totalDerecha + 75 + 'px');
      //console.log('entro');
    }
  }else{
    //console.log('es menor de 992px');
  }

  $('a[data-toggle="collapse"]').on('click', function () {
    setTimeout(function(){
      var $calculadoraAltura = $calculadoraMontaje.height();
      var $preguntasAltura = $preguntasFrecuentes.height();
      var $totalDerecha = $calculadoraAltura + $preguntasAltura;
      if($anchoWindow > 992) {
        //console.log('es mayor de 992px');
        //console.log($totalDerecha);
        $productos.css('min-height', $totalDerecha + 75 + 'px');
        //console.log('entro');
      }else{
        //console.log('es menor de 992px');
      }
    },0);
  });

  /* Funcionalidad para comprobar la altura del contenedor #main al cargar y al redimensionar elementos */
  /*var $contenedorProductosAlto = $('#main').height();
  var $alturaWindow = window.innerHeight;*/
  //console.log('El alto del contenedor de productos es '+ $contenedorProductosAlto);
  //console.log('El alto de la ventana del navegador es ' + $alturaWindow);
  /*if($alturaWindow < 800) {
    $('.contenedor-calculadora-montaje').css('padding','5px');
    $('.contenedor-calculadora-montaje .contenedor-botones').css('margin-top','5px');
    $('.contenedor-calculadora-montaje .contenedor-botones').css('margin-bottom','5px');
    $('.total-montaje').css('font-size','30px');
    $('.total-articulos').css('font-size','15px');
    $('.contenedor-botones .boton').css('padding','8px 30px 8px 30px');
    $('.contenedor-botones .boton').css('font-size','14px');
    $('.aviso').css('font-size','13px');
    $('.contenedor-preguntas-frecuentes h3').css('font-size','17px');
    $('.contenedor-preguntas-frecuentes a').css('font-size','13px');
    $('.contenedor-preguntas-frecuentes p').css('font-size','12px');
  }
  if($contenedorProductosAlto < 770){
    $('#sidebar').css('position','relative');
    $('#sidebar').css('right','-5px');
    $('#sidebar').css('padding-right','15px');
    $('#wrapper').css('margin-top','20px');
  }else{
    $('#sidebar').css('position','fixed');
    $('#sidebar').css('right','0');
    $('#sidebar').css('padding-right','26.3px');
    $('#wrapper').css('margin-top','0px');
  }*/
  /*$('a[data-toggle="collapse"]').on('click', function () {
    setTimeout(function(){
      var $contenedorProductosAlto = $('#main').height();
      //console.log('al redimensionar metio ' + $contenedorProductosAlto);
      if($contenedorProductosAlto < 770){
        $('#sidebar').css('position','relative');
        $('#sidebar').css('right','-5px');
        $('#sidebar').css('padding-right','15px');
        $('#wrapper').css('margin-top','20px');
      }else{
        $('#sidebar').css('position','fixed');
        $('#sidebar').css('right','0');
        $('#sidebar').css('padding-right','26.3px');
        $('#wrapper').css('margin-top','0px');
      }
    },601);
  });*/

  /* Funcionalidad para seleccionar todos los checkbox */
  $('#seleccionar-todo').click(function() {
    // Si esta seleccionado (si la propiedad checked es igual a true)
    if ($(this).prop('checked')) {
        // Selecciona cada input que tenga la clase .checar
        $('#main .checkbox').prop('checked', true);
    } else {
        // Deselecciona cada input que tenga la clase .checar
        $('#main .checkbox').prop('checked', false);
    }
});

  /* Funcionalidad spinner de boostrap custom */
  $(document).on('click', '.number-spinner span', function () { /* Evento click */
    var span = $(this),
    oldValue = span.closest('.number-spinner').find('input').val().trim(),
    newVal = 0,
    $iconPlus = span.closest('.number-spinner').find('span.glyphicon-plus'),
    $iconMinus = span.closest('.number-spinner').find('span.glyphicon-minus');
    $iconPlus.css('color','#000000');
    $iconMinus.css('color','#000000');
    if (span.attr('data-dir') == 'up') {
      if(oldValue >= 999){
        newVal = 999;
        span.css('color','#707070');
        return false;
      }
      newVal = parseInt(oldValue) + 1;
      span.css('color','#000000');
    } else {
      if (oldValue > 1) {
        newVal = parseInt(oldValue) - 1;
      } else {
        newVal = 1;
        $iconMinus.css('color','#707070');
      }
    }
    span.closest('.number-spinner').find('input').val(newVal);
    if(newVal >= 999) {
      newVal =  999;
      //console.log('entro en mayor que 999');
      span.closest('.number-spinner').find('input').val(newVal);
      $iconPlus.css('color','#707070');
      $iconMinus.css('color','#000000');
    }
    if(newVal <= 1) {
      //console.log('entro en menor igual 1');
      newVal =  1;
      span.closest('.number-spinner').find('input').val(newVal);
      $iconPlus.css('color','#000000');
      $iconMinus.css('color','#707070');
    }
  });
  /* Controlamos que  no se pase de valor 999 */
  $(document).on('keyup', '.number-spinner input', function () { /* Evento levantar dedo tecla */
    var $this = $(this);
    var $value = $this.val();
    var $glyPlus = $this.closest('.number-spinner').find('span.glyphicon-plus');
    var $glyMinus = $this.closest('.number-spinner').find('span.glyphicon-minus');
    if($value >= 999){
      $glyPlus.css('color','#707070');
      $glyMinus.css('color','#000000');
      $this = $(this).val(999);
    }else if($value <= 1){
      $glyPlus.css('color','#000000');
      $glyMinus.css('color','#707070');
      $this = $(this).val(1);
    }else{
      $glyPlus.css('color','#000000');
      $glyMinus.css('color','#000000');
    }
  });
  /* Controlamos en la carga los colores de los iconos */
  $('.number-spinner input').each(function(){
    var $this = $(this);
    var $valor = $this.val();
    if($valor = 1){
      $this.closest('.number-spinner').find('span.glyphicon-minus').css('color','#707070');
    }else if ($valor = 999){
      $this.closest('.number-spinner').find('span.glyphicon-plus').css('color','#707070');
    }
  });

  /* Funcionalidad para los iconos de los collapse para que roten segun apertura */
  $('.contenedor-listado-muebles a[data-toggle=collapse]').on('click',function(){
    var $this = $(this);
    var $icono = $this.find('.icon-arrow');
    //console.log($icono);
    if($icono.hasClass('down')){
      $icono.removeClass('down');
      $icono.addClass('up');
    }else{
      $icono.removeClass('up');
      $icono.addClass('down');
    }
  });
  /* Funcionalidad para cambiar icono */
  $('.contenedor-preguntas-frecuentes a[data-toggle="collapse"]').on('click',function(){
    $('.contenedor-preguntas-frecuentes .panel-collapse').each(function(){
      var $this= $(this);
      setTimeout(function(){
        if($this.hasClass('in')){
          $this.closest('.panel').find('img').removeClass('down');
          $this.closest('.panel').find('img').addClass('up');
        }else{
          $this.closest('.panel').find('img').removeClass('up');
          $this.closest('.panel').find('img').addClass('down');
        }
      },0);
    });
  });
  $('a.ver-articulo').on('click',function() {
    var $this = $(this);
    var $texto = $this.text();
    var $textoFormat = $texto.trim();
    //console.log($textoFormat);
    if($textoFormat =='Ver artículos'){
      $('a.ver-articulo').text('Ocultar artículos');
    }else if($textoFormat =='Seleccionar por producto'){

    } else{
      $('a.ver-articulo').text('Ver artículos');
    }
  });

  /* Función para hacer stycky el footer cuando hay cierto numero de elementos al cargar pagina*/
  var $listadoMuebles = $('.contenedor-listado-muebles').length;
  if($listadoMuebles >= 4) {
    //console.log('hay 4');
  }else{
    //console.log('no hay 4');
    $('.bottomMenu').addClass('fijo');
  }

  /************* Funcionalidad de la seccion de bienvenida *****************/

  $('a[data-target="#ver-instrucciones-detalladas"]').on('click',function() {
    //console.log('pulsaste enlace de ver instrucciones detalladas');
    var $this  = $(this);
    var $instrucciones = $('#ver-instrucciones-detalladas');
    setTimeout(function() {
      if($instrucciones.hasClass('in')){
        $this.find('img').removeClass('down');
        $this.find('img').addClass('up');
      }else{
        $this.find('img').removeClass('up');
        $this.find('img').addClass('down');
      }
    },0);
  });

  /* Funcionalidad para cambiar icono */
  $('.contenedor-preguntas-bienvenida a[data-toggle="collapse"]').on('click',function(){
    $('.contenedor-preguntas-bienvenida .panel-collapse').each(function(){
      var $this= $(this);
      setTimeout(function(){
        if($this.hasClass('in')){
          $this.closest('.panel').find('img').removeClass('down');
          $this.closest('.panel').find('img').addClass('up');
        }else{
          $this.closest('.panel').find('img').removeClass('up');
          $this.closest('.panel').find('img').addClass('down');
        }
      },0);
    });
  });

  /********************************** ELIMINAR A  PARTIR DE INTEGRACIÓN CON BACK ***********************************/

  /* Esta funcionalidad se podra reutilizar codigo para llamada a algunos mensajes. */

  /* Funcionalidad de prueba para contratar servicios*/
  $('#enviar-contrato-servicio').on('click',function(){
    //console.log('pulsaste contratación');
    $('#mensaje-formulario-contratacion').each(function(){
      var $this = $(this);
      var $valor = $this.val();
      //console.log('el campo' + 'tiene el valor :' + $valor);
      var check = checkCampos($this);
      if(check){
        setTimeout(function(){
          $('#mensaje-formulario-contratacion').modal('hide');
          $('#mensaje-contratacion-terminada').modal('show');
        },5000);
      }else{
        setTimeout(function(){
          $('#mensaje-formulario-contratacion').modal('hide');
          $('#mensaje-error').modal('show');
        },5000);
      }
    });

    function checkCampos(obj) {
      var camposRellenados = true;
      obj.find('input').each(function() {
        var $this = $(this);
        if( $this.val().length <= 0 ) {
            camposRellenados = false;
            return false;
        }
      });
      if(camposRellenados == false) {
          //console.log('paso 1');
          return false;
      }
      else {
          //console.log('paso 2')
          return true;
      }
    }

  });

});//end document.ready
